+++
pub_date = "2017-05-20T02:40:39+09:00"
mod_date = "2017-05-20T02:40:39+09:00"
title = "Terms"
description = "Terms of service"
+++
<div class="content">
<h2>Terms</h2>
<div id="terms"></div>
<p>This is a Imageboard for artists. Art has no language barrier. We welcome all contributions.</p>
<p>Please feel free to share your work, discuss techniques, and practice art together.</p>
<p>Our Services are not directed to children. Access to and use of our Services is only for those over the age of 13 (or 16 in the European Union). If you are younger than this, you may not register for or use our Services.</p>
    <section>
        <h3>1. Responsibility of Contributors</h3>
        <div>
            <p>If you post material to the Website, post links on the Website, or otherwise make (or allow any third party to make) material available by means of the Website (any such material, “Content”), You are entirely responsible for the content of, and any harm resulting from, that Content. That is the case regardless of whether the Content in question constitutes text, graphics, an audio file, or computer software. By making Content available, you represent and warrant that:</p>
            <ul>
            <li>the downloading, copying and use of the Content will not infringe the proprietary rights, including but not limited to the copyright, patent, trademark or trade secret rights, of any third party;</li>
            <li>if your employer has rights to intellectual property you create, you have either (i) received permission from your employer to post or make available the Content, including but not limited to any software, or (ii) secured from your employer a waiver as to all rights in or to the Content;</li>
            <li>you have fully complied with any third-party licenses relating to the Content, and have done all things necessary to successfully pass through to end users any required terms;</li>
            <li>the Content does not contain or install any viruses, worms, malware, Trojan horses or other harmful or destructive content;</li>
            <li>the Content is not spam, is not machine- or randomly-generated, and does not contain unethical or unwanted commercial content designed to drive traffic to third party sites or boost the search engine contributors of third party sites, or to further unlawful acts (such as phishing) or mislead recipients as to the source of the material (such as spoofing);</li>
            <li>the Content is not pornographic, does not contain threats or incite violence, and does not violate the privacy or publicity rights of any third party;</li>
            <li>your content is not getting advertised via unwanted electronic messages such as spam links on newsgroups, email lists, blogs and web sites, and similar unsolicited promotional methods;</li>
            <li>your content is not named in a manner that misleads your readers into thinking that you are another person or company; and</li>
            <li>you have, in the case of Content that includes computer code, accurately categorized and/or described the type, nature, uses and effects of the materials, whether requested to do so by World Fantasy Organization or otherwise.</li>
            </ul>
        </div>
    </section>
    <section>
        <h3>2. Responsibility of Website Visitors</h3>
        <p>World Fantasy Organization has not reviewed, and cannot review, all of the material, including computer software, posted to the Website, and cannot therefore be responsible for that material’s content, use or effects. By operating the Website, World Fantasy Organization does not represent or imply that it endorses the material there posted, or that it believes such material to be accurate, useful or non-harmful. You are responsible for taking precautions as necessary to protect yourself and your computer systems from viruses, worms, Trojan horses, and other harmful or destructive content. The Website may contain content that is offensive, indecent, or otherwise objectionable, as well as content containing technical inaccuracies, typographical mistakes, and other errors. The Website may also contain material that violates the privacy or publicity rights, or infringes the intellectual property and other proprietary rights, of third parties, or the downloading, copying or use of which is subject to additional terms and conditions, stated or unstated. World Fantasy Organization disclaims any responsibility for any harm resulting from the use by visitors of the Website, or from any downloading by those visitors of content there posted.</p>
    </section>
        <section>
        <h3>3. Changes</h3>
        <p>World Fantasy Organization reserves the right, at its sole discretion, to modify or replace any part of this Agreement. It is your responsibility to check this Agreement periodically for changes. Your continued use of or access to the Website following the posting of any changes to this Agreement constitutes acceptance of those changes. World Fantasy Organization may also, in the future, offer new services and/or features through the Website (including, the release of new tools and resources). Such new features and/or services shall be subject to the terms and conditions of this Agreement.</p>
    </section>
    <section>
        <h3>4. Copyright Terms</h3>
        <p>You retain sole ownership of your content.</p>
    </section>
    <section>
        <h3>5. This document is CC-BY-SA</h3>
        <p>Originally adapted from the WordPress Terms of Service(https://en.wordpress.com/tos/) and Discourse Terms of Service(https://meta.discourse.org/tos). Last updated July 22, 2018.</p>
    </section>
</div>
