+++
date = "2018-07-09T11:18:15.665Z"
pub_date = "2018-07-09T11:18:15.665Z"
mod_date = "2018-07-09T11:18:15.665Z"
title = "4069441583685246153"
description = "講評失礼いたします。可愛らしい絵柄ですね。横顔はとても描き手様の個性が出ますので、自分の理想に近いイラストレーターさんや漫画家さんがどのように表現されているか観察してみてください。バランスが悪いなどはありませんが、もう少し鼻と口のラインを出してもいいと思います。首に関しても特に気になるところはありません。頭のサイズ感はもう少し控えめで良いかと思いましたが、こちらも絵柄に左右されるところですのでお好きなバランスを研究してみてください。全体的に丁寧で綺麗に描かれているとおもいます。髪の毛に関しては毛束が大きすぎてのっぺりとした印象です。人間のつむじや、髪の分け目を意識しましょう。また、毛先は全て同じ方向にハネさせるのではなく、ランダムに反対にハネた髪をいれると髪に動きが出て自然に見えます。内巻きなど毛先が揃った髪型の際も流れに反した遊び髪を数本入れてあげるとぐっと雰囲気が良くなるので是非試してみてください。髪型にもよりますが横向きですので現実的には向こう側の髪が見えない場合も向こう側に見える髪を描いてあげると遠近感が出ます。こちらも取り入れてみてください。"
original_id = "4068430643682322231"
original_tag = ["4068430643682322231"]
content_Type = "critique-artwork"
contributors = ["k"]
name = "k"
website = ""
+++
講評失礼いたします。可愛らしい絵柄ですね。横顔はとても描き手様の個性が出ますので、自分の理想に近いイラストレーターさんや漫画家さんがどのように表現されているか観察してみてください。
バランスが悪いなどはありませんが、もう少し鼻と口のラインを出してもいいと思います。
首に関しても特に気になるところはありません。頭のサイズ感はもう少し控えめで良いかと思いましたが、こちらも絵柄に左右されるところですのでお好きなバランスを研究してみてください。全体的に丁寧で綺麗に描かれているとおもいます。
髪の毛に関しては毛束が大きすぎてのっぺりとした印象です。
人間のつむじや、髪の分け目を意識しましょう。また、毛先は全て同じ方向にハネさせるのではなく、ランダムに反対にハネた髪をいれると髪に動きが出て自然に見えます。
内巻きなど毛先が揃った髪型の際も流れに反した遊び髪を数本入れてあげるとぐっと雰囲気が良くなるので是非試してみてください。
髪型にもよりますが横向きですので現実的には向こう側の髪が見えない場合も向こう側に見える髪を描いてあげると遠近感が出ます。こちらも取り入れてみてください。