+++
date = "2018-07-12T01:46:45.807Z"
pub_date = "2018-07-12T01:46:45.807Z"
mod_date = "2018-07-12T01:46:45.807Z"
title = "4071691639413848851"
description = "・髪の塗り、描き方について髪に流れが感じられないのが違和感です。つむじから毛先に一本一本流れているのを意識して描いてみるといいかと思います。髪の光も頭の丸みを意識して入れるといいです。・服の明暗について全体的に明るい印象です。暗いところはより暗く、明るいところはより明るくすると画面にメリハリがでていいかと思います。色が暗く地味になっていると感じる場合は反射光を入れると質感や立体感もでて、色にも幅が出ます。線画も綺麗でキャラもとても可愛いので質感や明暗に注意するとより素敵になると思います。"
original_id = "4070757023681256388"
original_tag = ["4070757023681256388"]
content_Type = "critique-artwork"
contributors = ["t"]
name = "t"
website = ""
+++
・髪の塗り、描き方について
髪に流れが感じられないのが違和感です。つむじから毛先に一本一本流れているのを意識して描いてみるといいかと思います。髪の光も頭の丸みを意識して入れるといいです。
・服の明暗について
全体的に明るい印象です。暗いところはより暗く、明るいところはより明るくすると画面にメリハリがでていいかと思います。
色が暗く地味になっていると感じる場合は反射光を入れると質感や立体感もでて、色にも幅が出ます。
線画も綺麗でキャラもとても可愛いので質感や明暗に注意するとより素敵になると思います。